import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// services
import { AuthService } from '../../services/auth.service';
import { TipoTransaccionService } from '../../services/tipo-transaccion-service';
import { MediosPagoService } from '../../services/medios-pago-service';
import { CategoriasService } from 'src/app/services/categorias-service';
import { CuentasService } from '../../services/cuentas-service';
import { TransaccionesService } from 'src/app/services/transacciones-service';

// models
import { UserInterface } from '../../models/user-interface';
import { TipoTransaccionesModel } from '../../models/tipo-transaccion-model';
import { MediosPagoModel } from '../../models/medio-pago-model';
import { CategoriasModel } from '../../models/categorias-model';
import { CuentasModel } from '../../models/cuentas-model';
import { TransaccionesModel } from 'src/app/models/transaccion-model';
import { TransaccionesResponseModel } from '../../models/transaccion-response-model';



@Component({
  selector: 'app-transacciones',
  templateUrl: './transacciones.component.html',
  styleUrls: ['./transacciones.component.css']
})
export class TransaccionesComponent implements OnInit {

  user: UserInterface = {
    username: '',
    clave: '',
    id: ''
  }; 

  titulo = 'Listado de Transacciones'; 
  isEvent = false;
  eventMessage = '';
  eventColor = '';
  tipoTransaccionesList: TipoTransaccionesModel[] = [];
  mediosPagoList: MediosPagoModel[] = [];
  categoriasList: CategoriasModel[] = [];
  cuentasList: CuentasModel[]= [];
  transaccionesResponse: TransaccionesResponseModel[] =[];
  singleReportColumns = ['No.Transaccion', 'Tipo Transaccion', 'No Cuenta', 'Total', 'Fecha', 'Medio Pago', 'Categoria'];
  formModal: FormGroup;

  constructor(private authServices: AuthService, 
              private tipoTransaccionService: TipoTransaccionService,
              private mediosPagoService: MediosPagoService,
              private categoriasService: CategoriasService,
              private cuentasService: CuentasService,
              private transaccionesService: TransaccionesService,
              config: NgbModalConfig, 
              private modalService: NgbModal, 
              private formBuilder: FormBuilder) { 
    // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;

    this.formModal = this.formBuilder.group({
      cuenta : '0',
      tipoTransaccion: '0',
      medioPago: '0',
      categoria: '0',
      total : '0'
    });

  }

  ngOnInit(): void {
    this.user = this.authServices.getCurrentUser();
    this.getTipoTransaccion();
    this.getMedioPago();
    this. getCuentas();
    this.getTransaciones();
    
    this.formModal.controls['tipoTransaccion'].valueChanges.subscribe((selectedValue: number) => {
      this.getCategorias();
    });
  }


  // ******************** Modal *****************************
  open(content: any) {
    this.modalService.open(content);
  }

  // ******************** Consulta Datos *****************************
  getTipoTransaccion(){
    this.tipoTransaccionService.getTipoTransaccionesAll().subscribe(response => {

      if ( response == null || Object.entries(response).length === 0 ) {
        console.log('data null');
        this.eventMessage = 'No se encontro informacion';
        this.eventColor = 'alert-info';
        this.event();

      } else {
        let resul = JSON.parse(JSON.stringify(response));
        this.tipoTransaccionesList = resul['tipoTransaccion'];
        console.log('lista tipo transacciones: ', this.tipoTransaccionesList);
      }

    }, error => {
      console.log('Error: ',error)
      this.eventMessage = 'Se presento un error consultando tipo de transacciones';
      this.eventColor = 'alert-danger';
      this.event();
    });
  }

  getMedioPago(){
    this.mediosPagoService.getMediosPagoAll().subscribe(response => {

      if ( response == null || Object.entries(response).length === 0 ) {
        console.log('data null');
        this.eventMessage = 'No se encontro informacion';
        this.eventColor = 'alert-info';
        this.event();

      } else {
        let resul = JSON.parse(JSON.stringify(response));
        this.mediosPagoList = resul['mediosPago'];
        console.log('lista medios de pago: ', this.mediosPagoList);
      }

    }, error => {
      console.log('Error: ',error)
      this.eventMessage = 'Se presento un error consultando medios de pago';
      this.eventColor = 'alert-danger';
      this.event();
    });
  }

  getCategorias(){
    this.categoriasService.getCategoriasAll(this.formModal.controls['tipoTransaccion'].value).subscribe(response => {

      if ( response == null || Object.entries(response).length === 0 ) {
        console.log('data null');
        this.eventMessage = 'No se encontro informacion';
        this.eventColor = 'alert-info';
        this.event();

      } else {
        let resul = JSON.parse(JSON.stringify(response));
        this.categoriasList = resul['result'];
        console.log('lista categorias: ', this.categoriasList);
      }

    }, error => {
      console.log('Error: ',error)
      this.eventMessage = 'Se presento un error consultando categorias';
      this.eventColor = 'alert-danger';
      this.event();
    });
  }

  getCuentas() {
    this.cuentasService.getCuentas(this.user.id).subscribe(response => {
      
      if ( response == null || Object.entries(response).length === 0 ) {
        console.log('data null');
        this.eventMessage = 'No se encontro informacion';
        this.eventColor = 'alert-info';
        this.event();
      } else {
        let resul = JSON.parse(JSON.stringify(response));
        this.cuentasList = resul['cuentas'];
        console.log('lista cuentas: ', this.cuentasList);
      }
    }, error => {
      console.log('Error: ',error)
      this.eventMessage = 'Se presento un error consultando cuentas de usuario';
      this.eventColor = 'alert-danger';
      this.event();
    });
  }

  // ******************** Logica *****************************
 
  getTransaciones() {
    this.transaccionesService.getTransacciones(this.user.id).subscribe(response => {
      
      if ( response == null || Object.entries(response).length === 0 ) {
        console.log('data null');
        this.eventMessage = 'No se encontro informacion';
        this.eventColor = 'alert-info';
        this.event();
      } else {
        let resul = JSON.parse(JSON.stringify(response));
        this.transaccionesResponse = resul['transacciones'];
        console.log('lista transacciones: ', this.transaccionesResponse);
      }
    }, error => {
      console.log('Error: ',error)
      this.eventMessage = 'Se presento un error consultando transacciones';
      this.eventColor = 'alert-danger';
      this.event();
    });
  }

  guardarTransaccion(){
    this.modalService.dismissAll();

    if (+this.formModal.controls['cuenta'].value > 0 && +this.formModal.controls['tipoTransaccion'].value > 0 
      && +this.formModal.controls['medioPago'].value > 0 && +this.formModal.controls['categoria'].value > 0 
      && +this.formModal.controls['total'].value > 0 ) {

        let transaccion: TransaccionesModel= {
          id: 0,
          cuentaId: this.formModal.controls['cuenta'].value,
          tipoTransaccionId: this.formModal.controls['tipoTransaccion'].value,
          medioId: this.formModal.controls['medioPago'].value,
          categoriaId: this.formModal.controls['categoria'].value,
          total: this.formModal.controls['total'].value,
          fecha: new Date
        };

        this.transaccionesService.saveTransaccion(transaccion).subscribe(response => {
          if ( response == null || Object.entries(response).length === 0 ) {
            console.log('data null');
          } else {
            let resul = JSON.parse(JSON.stringify(response));
            console.log('Resultado: ', resul);
            this.getTransaciones();
            
            this.event();
            this.eventMessage = resul['resultado'];
            this.eventColor = 'alert-success';
          }
        });

        this.formModal.controls['cuenta'].setValue('0');
        this.formModal.controls['tipoTransaccion'].setValue('0');
        this.formModal.controls['medioPago'].setValue('0');
        this.formModal.controls['categoria'].setValue('0');
        this.formModal.controls['total'].setValue('0');

    } else {
      this.eventMessage = 'Datos incorrectos';
      this.eventColor = 'alert-danger';
      this.event();
    }


  }

  // ******************** Control Eventos *****************************
  event(): void {
    this.isEvent = true;
    setTimeout(() => {
      this.isEvent = false;
    }, 10000);
  }

}
