import { Component, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';

// services
import { AuthService } from '../../services/auth.service';

// models
import { UserInterface } from '../../models/user-interface';
import { GraficasService } from '../../services/graficas-service';
import { ReporteResponseModel } from '../../models/reporte-response-model';

@Component({
  selector: 'app-graficas',
  templateUrl: './graficas.component.html',
  styleUrls: ['./graficas.component.css']
})
export class GraficasComponent implements OnInit {

  titulo = 'Estadisticas'; 

  user: UserInterface = {
    username: '',
    clave: '',
    id: ''
  }; 

  isEvent = false;
  eventMessage = '';
  eventColor = '';
  reporteTipo: ReporteResponseModel[] = [];
  datatipoList: string [] = [];
  valuetipoList: number [] = [];
  dataCategoriaList: string [] = [];
  valueCategoriaList: number [] = [];

  // Doughnut
  public doughnutChartLabels: Label[] = ['Sin informacion'];
  public doughnutChartData: number[] = [100];
  public doughnutChartType: ChartType = 'doughnut'

  public doughnutChartLabels2: Label[] = ['Sin informacion'];
  public doughnutChartData2: number[] = [100];
  public doughnutChartType2: ChartType = 'doughnut'

  constructor(private authServices: AuthService, private graficasService: GraficasService) { }

  ngOnInit(): void {
    this.user = this.authServices.getCurrentUser();
    this.getTransaciones();
    this.getCategorias();
  }

  // ******************** Graficas *****************************
  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

    // ******************** Logica *****************************
 
  getTransaciones() {
    this.graficasService.getTipo(this.user.id).subscribe(response => {
      
      if ( response == null || Object.entries(response).length === 0 ) {
        console.log('data null');
        this.eventMessage = 'No se encontro informacion';
        this.eventColor = 'alert-info';
        this.event();
      } else {
        let resul = JSON.parse(JSON.stringify(response));
        this.reporteTipo = resul['resultado'];
        console.log('Reporte tipo: ', this.reporteTipo);
        
        
        this.reporteTipo.forEach(report => {
          this.datatipoList.push(report.data);
          this.valuetipoList.push(report.total)
        });

        this.doughnutChartLabels = this.datatipoList;
        this.doughnutChartData = this.valuetipoList;

      }
    }, error => {
      console.log('Error: ',error)
      this.eventMessage = 'Se presento un error consultando reporte';
      this.eventColor = 'alert-danger';
      this.event();
    });
  }

  getCategorias() {
    this.graficasService.getCategorias(this.user.id).subscribe(response => {
      
      if ( response == null || Object.entries(response).length === 0 ) {
        console.log('data null');
        this.eventMessage = 'No se encontro informacion';
        this.eventColor = 'alert-info';
        this.event();
      } else {
        let resul = JSON.parse(JSON.stringify(response));
        this.reporteTipo = resul['resultado'];
        console.log('Reporte tipo: ', this.reporteTipo);
        
        
        this.reporteTipo.forEach(report => {
          this.dataCategoriaList.push(report.data);
          this.valueCategoriaList.push(report.total)
        });

        this.doughnutChartLabels2 = this.dataCategoriaList;
        this.doughnutChartData2 = this.valueCategoriaList;

      }
    }, error => {
      console.log('Error: ',error)
      this.eventMessage = 'Se presento un error consultando reporte';
      this.eventColor = 'alert-danger';
      this.event();
    });
  }

  // ******************** Control Eventos *****************************
  event(): void {
    this.isEvent = true;
    setTimeout(() => {
      this.isEvent = false;
    }, 10000);
  }

}
