import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UserInterface } from 'src/app/models/user-interface';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, private location: Location) { }
  
  user: UserInterface = {
    username: '',
    clave: '',
    id: ''
  };
  
  public isError = false;

  ngOnInit() { }

  onLogin(form: NgForm) {
    if (form.valid) {
      let userTmp: any = {
        username: this.user.username,
        clave: this.user.clave
      };

      return this.authService.loginuser(userTmp).subscribe(data => {
        
        if (data == null || Object.entries(data).length === 0 ) {
          console.log('data null');
        } else {
          let resul = JSON.parse(JSON.stringify(data));
          let userTmp: UserInterface = resul['usuario'];
                    
          if (userTmp.username == this.user.username) {
            this.authService.setUser(userTmp);
            window.location.href = 'cuentas';
            this.isError = false;
          } 
        }

      },  error => this.onIsError()
      );
    } else {
      console.log('Formulario no valido');
      return null;
    }
  }

  onIsError(): void {
    this.isError = true;
    setTimeout(() => {
      this.isError = false;
    }, 4000);
  }

}
