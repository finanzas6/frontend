import { Component, OnInit} from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


// services
import { AuthService } from '../../services/auth.service';
import { CuentasService } from '../../services/cuentas-service';


// models
import { UserInterface } from '../../models/user-interface';
import { CuentasModel } from '../../models/cuentas-model';

@Component({
  selector: 'app-cuentas',
  templateUrl: './cuentas.component.html',
  styleUrls: ['./cuentas.component.css'],
  providers: [NgbModalConfig, NgbModal]
})
export class CuentasComponent implements OnInit {

  formModal: FormGroup;
  
  titulo = 'Listado de Cuentas';

  user: UserInterface = {
    username: '',
    clave: '',
    id: ''
  }; 

  singleReportColumns = ['No.Cuenta', 'Descripcion', 'Propietario', 'Id Usuario', 'Total Ingresos', 'Total Gastos'];
  cuentasList: CuentasModel[]= [];

  isEvent = false;
  eventMessage = '';
  eventColor = '';

  constructor(private authServices: AuthService, private cuentasService: CuentasService, config: NgbModalConfig, private modalService: NgbModal, private formBuilder: FormBuilder) { 
    // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;

    this.formModal = this.formBuilder.group({
      descripcion : ['', [Validators.required, Validators.minLength(4)]],
      saldo : ['0']
    });

  }

  ngOnInit(): void {
    this.user = this.authServices.getCurrentUser();
    console.log('Usuario logueado: ', this.user);

    this.populateCuentas();
  }

  populateCuentas() {
    this.cuentasService.getCuentas(this.user.id).subscribe(response => {
      
      console.log('Cuentas: ', response)
      
      if ( response == null || Object.entries(response).length === 0 ) {
        console.log('data null');
        this.eventMessage = 'No se encontro informacion';
        this.eventColor = 'alert-info';
        this.event();
      } else {
        let resul = JSON.parse(JSON.stringify(response));
        this.cuentasList = resul['cuentas'];
        console.log('lista cuentas: ', this.cuentasList);
      }
    }, error => {
      console.log('Error: ',error) 
      this.eventMessage = 'Se presento un error consultando transacciones';
      this.eventColor = 'alert-danger';
      this.event();
    });
  }

  open(content: any) {
    this.modalService.open(content);
  }

  guardarCuenta(){
    this.modalService.dismissAll();

    if (this.formModal.controls['descripcion'].value != '' && this.formModal.controls['saldo'].value > 0 ) {
      let cuentaTmp: CuentasModel = {
        id: 0,
        descripcion: this.formModal.controls['descripcion'].value,
        usuarioId: +this.user.id,
        totalGastos: 0,
        totalIngresos: this.formModal.controls['saldo'].value
      };
  
      console.log('Datos a guardar: ', cuentaTmp);
      
      this.cuentasService.saveCuenta(cuentaTmp).subscribe(response => {
        
        if ( response == null || Object.entries(response).length === 0 ) {
          console.log('data null');
        } else {
          let resul = JSON.parse(JSON.stringify(response));
          console.log('Resultado: ', resul);
          this.populateCuentas();
          
          this.event();
          this.eventMessage = resul['resultado'];
          this.eventColor = 'alert-success';
        }
  
      });
    } else {
      this.eventMessage = 'Datos incorrectos';
      this.eventColor = 'alert-danger';
      this.event();
    }

    
    this.formModal.controls['descripcion'].setValue('');
    this.formModal.controls['saldo'].setValue(0);
  }

  event(): void {
    this.isEvent = true;
    setTimeout(() => {
      this.isEvent = false;
    }, 10000);
  }

}
