import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// services
import { AuthService } from '../../services/auth.service';
import { CategoriasService } from 'src/app/services/categorias-service';
import { TipoTransaccionService } from '../../services/tipo-transaccion-service';

// models
import { UserInterface } from '../../models/user-interface';
import { TipoTransaccionesModel } from '../../models/tipo-transaccion-model';
import { CategoriasResponseModel } from '../../models/categoria-response-model';
import { CategoriasRequestModel } from '../../models/categoria-request-model';
import { DeleteRequestModel } from '../../models/delete-request';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {

  user: UserInterface = {
    username: '',
    clave: '',
    id: ''
  }; 

  titulo = 'Listado de Categorias'; 
  isEvent = false;
  eventMessage = '';
  eventColor = '';
  singleReportColumns = ['No.categoria', 'Descripcion', 'Tipo Transaccion', 'Eliminar'];
  formModal: FormGroup;
  formTable: FormGroup;
  tipoTransaccionesList: TipoTransaccionesModel[] = [];
  categoriasResponse: CategoriasResponseModel[] = [];

  constructor(
    private authServices: AuthService,
    private categoriasService: CategoriasService,
    private tipoTransaccionService: TipoTransaccionService,
    config: NgbModalConfig, 
    private modalService: NgbModal, 
    private formBuilder: FormBuilder
    ) { 
      // customize default values of modals used by this component tree
      config.backdrop = 'static';
      config.keyboard = false;

      this.formModal = this.formBuilder.group({
        descripcion : '',
        tipoTransaccion: '0'
      });
      this.formTable = this.formBuilder.group({
        data : ''
      });
    }

  ngOnInit(): void {
    this.user = this.authServices.getCurrentUser();
    this.getTipoTransaccion();
    this.getCategoriasUsuario();
  }

  // ******************** Modal *****************************
  open(content: any) {
    this.modalService.open(content);
  }

  // ******************** Consulta Datos *****************************
  getTipoTransaccion(){
    this.tipoTransaccionService.getTipoTransaccionesAll().subscribe(response => {

      if ( response == null || Object.entries(response).length === 0 ) {
        console.log('data null');
        this.eventMessage = 'No se encontro informacion';
        this.eventColor = 'alert-info';
        this.event();

      } else {
        let resul = JSON.parse(JSON.stringify(response));
        this.tipoTransaccionesList = resul['tipoTransaccion'];
        console.log('lista tipo transacciones: ', this.tipoTransaccionesList);
      }

    }, error => {
      console.log('Error: ',error)
      this.eventMessage = 'Se presento un error consultando tipo de transacciones';
      this.eventColor = 'alert-danger';
      this.event();
    });
  }

  // ******************** Logica *****************************
  
  getCategoriasUsuario(){
    this.categoriasService.getCategoriasByUsuario(this.user.id).subscribe(response => {
      
      if ( response == null || Object.entries(response).length === 0 ) {
        console.log('data null');
        this.eventMessage = 'No se encontro informacion';
        this.eventColor = 'alert-info';
        this.event();
      } else {
        let resul = JSON.parse(JSON.stringify(response));
        this.categoriasResponse = resul['categorias'];
        console.log('lista de categorias: ', this.categoriasResponse);
      }
    }, error => {
      console.log('Error: ',error)
      this.eventMessage = 'Se presento un error consultando categorias';
      this.eventColor = 'alert-danger';
      this.event();
    });
  }

  guardarCategoria(){

    this.modalService.dismissAll();

    if (this.formModal.controls['descripcion'].value != '' && +this.formModal.controls['tipoTransaccion'].value > 0 ) {

        let categoriaTmp: CategoriasRequestModel = {
          tipoTransaccionId: this.formModal.controls['tipoTransaccion'].value,
          descripcion: this.formModal.controls['descripcion'].value,
          usuarioId: +this.user.id
        };

        this.categoriasService.saveCategoria(categoriaTmp).subscribe(response => {
          if ( response == null || Object.entries(response).length === 0 ) {
            console.log('data null');
            this.eventMessage = 'No se encontro informacion';
            this.eventColor = 'alert-info';
            this.event();
          } else {
            let resul = JSON.parse(JSON.stringify(response));
            console.log('Resultado: ', resul);
            this.getCategoriasUsuario();
            
            this.event();
            this.eventMessage = resul['resultado'];
            this.eventColor = 'alert-success';
          }
        });

        this.formModal.controls['descripcion'].setValue('');
        this.formModal.controls['tipoTransaccion'].setValue('0');

    } else {
      this.eventMessage = 'Datos incorrectos';
      this.eventColor = 'alert-danger';
      this.event();
    }

  }

  eliminarCategoria(item: CategoriasResponseModel){
    let cat: DeleteRequestModel = {
      usuarioId: +this.user.id,
      categoria: item.descripcion,
      tipo: item.nombre
    };

    this.categoriasService.deleteCategoria(cat).subscribe(response => {
      if ( response == null || Object.entries(response).length === 0 ) {
        console.log('data null');
        this.eventMessage = 'No se encontro informacion';
        this.eventColor = 'alert-info';
        this.event();
      } else {
        let resul = JSON.parse(JSON.stringify(response));
        console.log('Resultado: ', resul);
        this.getCategoriasUsuario();
        
        this.event();
        this.eventMessage = resul['resultado'];
        this.eventColor = 'alert-success';
      }
    });

  }

  // ******************** Control Eventos *****************************
  event(): void {
    this.isEvent = true;
    setTimeout(() => {
      this.isEvent = false;
    }, 10000);
  }

}
