import { Component, OnInit } from '@angular/core';
import { UserInterface } from '../../models/user-interface';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(private authService: AuthService) { }

  user: UserInterface = {
    username: 'null',
    clave: '',
    id: ''
  };
  ngOnInit(): void {
    this.user = this.authService.getCurrentUser();
  }

}
