export interface ReporteResponseModel {
    total: number;
    data: string;
}