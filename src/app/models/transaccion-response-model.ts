export interface TransaccionesResponseModel {
    id: number;
    tipoTransaccion: string;
    cuentaId: number;
    total: number;
    fecha: Date;
    medioPago: string;
    categoria: string;
}