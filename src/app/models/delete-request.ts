export interface DeleteRequestModel {
    usuarioId: number;
    tipo: string;
    categoria: string;
}