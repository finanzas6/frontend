export interface CategoriasRequestModel {
    tipoTransaccionId: number;
    descripcion: string;
    usuarioId: number;
}