export interface CategoriasResponseModel {
    id: number;
    descripcion: string;
    nombre: string;
}