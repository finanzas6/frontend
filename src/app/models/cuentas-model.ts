export interface CuentasModel {
    id: number;
    usuarioId: number;
    totalIngresos: number;
    totalGastos: number;
    descripcion: string;
}