export interface CategoriasUsuarioModel {
    id: number;
    usuarioId: number;
    categoriaId: number;
}