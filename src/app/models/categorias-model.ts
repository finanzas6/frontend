export interface CategoriasModel {
    tipoTransaccionId: number;
    descripcion: string;
    id: number;
}