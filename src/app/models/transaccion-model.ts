export interface TransaccionesModel {
    id: number;
    tipoTransaccionId: number;
    cuentaId: number;
    total: number;
    fecha: Date;
    medioId: number;
    categoriaId: number;
}