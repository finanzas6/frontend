export interface UserInterface {
    id: string;
    nombre?: string;
    apellido?: string;
    correo?: string;
    clave: string;
    username: string;
}