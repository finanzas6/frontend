import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { UserInterface } from '../models/user-interface';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) { }
  canActivate() {
    let user: UserInterface = this.authService.getCurrentUser();
    if (user.username !== "null") {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}