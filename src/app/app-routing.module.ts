import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { CuentasComponent } from './components/cuentas/cuentas.component';
import { AuthGuard } from './guards/auth.guard';
import { TransaccionesComponent } from './components/transacciones/transacciones.component';
import { CategoriasComponent } from './components/categorias/categorias.component';
import { GraficasComponent } from './components/graficas/graficas.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'cuentas', component: CuentasComponent, canActivate: [AuthGuard] },
  { path: 'transacciones', component: TransaccionesComponent, canActivate: [AuthGuard] },
  { path: 'categorias', component: CategoriasComponent, canActivate: [AuthGuard] },
  { path: 'graficas', component: GraficasComponent, canActivate: [AuthGuard] },
  { path: '**', pathMatch: 'full', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
