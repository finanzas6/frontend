import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({providedIn: "root"})
export class GraficasService {
    
  constructor(private http: HttpClient) {}
  
  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  });

  getTipo(usuarioId: string) {
      return this.http.get('http://localhost:8081/transaccion/reportTipo/' + usuarioId);
  }

  getCategorias(usuarioId: string) {
    return this.http.get('http://localhost:8081/transaccion/reportCategoria/' + usuarioId);
  }

}