import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({providedIn: "root"})
export class TipoTransaccionService {
    
  constructor(private http: HttpClient) {}
  
  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  });

  getTipoTransaccionesAll() {
      return this.http.get('http://localhost:8081/tipoTransaccion/get');
  }

}