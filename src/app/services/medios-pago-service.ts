import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({providedIn: "root"})
export class MediosPagoService {
    
  constructor(private http: HttpClient) {}
  
  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  });

  getMediosPagoAll() {
      return this.http.get('http://localhost:8081/mediosPagos/get');
  }

}