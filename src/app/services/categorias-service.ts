import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CategoriasRequestModel } from '../models/categoria-request-model';
import { DeleteRequestModel } from '../models/delete-request';

@Injectable({providedIn: "root"})
export class CategoriasService {
    
  constructor(private http: HttpClient) {}
  
  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  });

  getCategoriasAll(tipo: number) {
      return this.http.get('http://localhost:8081/categoria/tipo/' + tipo);
  }

  getCategoriasByUsuario(usuarioId: string) {
    return this.http.get('http://localhost:8081/categoria/get/' + usuarioId);
  }

  saveCategoria(categoriaRequest: CategoriasRequestModel) {
    return this.http.post('http://localhost:8081/categoria/crear', categoriaRequest, {headers: this.headers});
  }

  deleteCategoria(deleteRequestModel: DeleteRequestModel) {
    return this.http.post('http://localhost:8081/categoria/delete', deleteRequestModel, {headers: this.headers});
  }

}