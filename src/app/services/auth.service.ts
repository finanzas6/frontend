
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { UserInterface } from '../models/user-interface';

@Injectable({providedIn: "root"})
export class AuthService {
    
    constructor(private http: HttpClient) {}
    
    headers: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json"
    });

    userTmp: UserInterface = {
        username: 'null',
        clave: 'null',
        id: ''
    };

    loginuser(userTmp: any) {
        return this.http.post('http://localhost:8081/login', userTmp, {headers: this.headers});
    }

    setUser(user: UserInterface): void {
        let user_string = JSON.stringify(user);
        localStorage.setItem("currentUser", user_string);
    }

    getCurrentUser(): UserInterface {
        let user_string = localStorage.getItem("currentUser");
        if (user_string !== null || user_string === undefined) {
          let user: UserInterface = JSON.parse(user_string);
          return user;
        } else {
          return this.userTmp;
        }
    }
    
    logoutUser() {
        localStorage.removeItem("currentUser");
        window.location.href = 'login';
    }

}