import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { TransaccionesModel } from '../models/transaccion-model';

@Injectable({providedIn: "root"})
export class TransaccionesService {
    
  constructor(private http: HttpClient) {}
  
  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  });

  getTransacciones(usuarioId: string) {
      return this.http.get('http://localhost:8081/transaccion/getTransaccion/' + usuarioId);
  }

  saveTransaccion(transaccion: TransaccionesModel) {
    return this.http.post('http://localhost:8081/transaccion/crear', transaccion, {headers: this.headers});
  }

}