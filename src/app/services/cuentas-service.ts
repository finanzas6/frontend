import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CuentasModel } from '../models/cuentas-model';

@Injectable({providedIn: "root"})
export class CuentasService {
    
  constructor(private http: HttpClient) {}
  
  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  });

  getCuentas(usuarioId: string) {
      return this.http.get('http://localhost:8081/cuenta/getCuentas/' + usuarioId);
  }

  saveCuenta(cuenta: CuentasModel) {
    return this.http.post('http://localhost:8081/cuenta/crear', cuenta, {headers: this.headers});
  }

}